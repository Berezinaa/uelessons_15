﻿// Homework15.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>

void PrintNumbers(int Limit, bool IsOdd) {
	std::string isOdd = (IsOdd) ? "Odd" : "Even";
	std::cout << "\n" << isOdd << " numbers from 0 to " << Limit << "\n";
	int lowBordrer = (IsOdd) ? 1 : 0;

	for (size_t i = lowBordrer; i <= Limit; i = i + 2)
	{
		std::cout << i << "\n";
	}
}

int main()
{
    int upperBorder = 99;
    std::cout << "\n" << "Odd numbers from 0 to " << upperBorder << "\n";

	for (size_t i = 1; i <= upperBorder; i=i+2)
	{
		std::cout << i << "\n";
	}

	PrintNumbers(99, true);
	PrintNumbers(99, false);
}

// Запуск программы: CTRL+F5 или меню "Отладка" > "Запуск без отладки"
// Отладка программы: F5 или меню "Отладка" > "Запустить отладку"

// Советы по началу работы 
//   1. В окне обозревателя решений можно добавлять файлы и управлять ими.
//   2. В окне Team Explorer можно подключиться к системе управления версиями.
//   3. В окне "Выходные данные" можно просматривать выходные данные сборки и другие сообщения.
//   4. В окне "Список ошибок" можно просматривать ошибки.
//   5. Последовательно выберите пункты меню "Проект" > "Добавить новый элемент", чтобы создать файлы кода, или "Проект" > "Добавить существующий элемент", чтобы добавить в проект существующие файлы кода.
//   6. Чтобы снова открыть этот проект позже, выберите пункты меню "Файл" > "Открыть" > "Проект" и выберите SLN-файл.
